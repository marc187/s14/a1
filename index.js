let firstName = `John`;
let lastName = `Smith`;
let age = `30`;
let hobbies = [`Biking`, `Mountain Climbing`, `Swimming`]
let isMarried = true;

let workAddress = {
    houseNumber: `32`,
    street: `Washington`,
    city: `Lincoln`,
    state: `Nebraska`
}

function printUserInfo(){
    console.log(`First Name: ${firstName}`);
    console.log(`Last Name: ${lastName}`);
    console.log(`Age: ${age}`);
    console.log(`Hobbies:`);
    console.log(hobbies);
    console.log(`Work Address:`);
    console.log(workAddress);
}

function returnFunction(_firstName, _lastName, _age, _hobbies, _workAddress, _isMarried){
    console.log(`${_firstName} ${_lastName} is ${_age} years of age.`);
    console.log(`This was printed inside of the function.`);
    console.log(_hobbies);
    console.log(`This was printed inside of the function.`);
    console.log(_workAddress);
    console.log(`The value of isMarried is ${isMarried}`);
}

printUserInfo();
returnFunction(firstName, lastName, age, hobbies, workAddress, isMarried);